$("#navbar-template").load("common/header.html");
$("footer").load("common/footer.html");

$(document).ready(function() {
    // $('.header-link li.nav-item a').click(function(e) {
    //     e.preventDefault();
    //     $('a.nav-link').removeClass('active');
    //     $(this).addClass('active');
    // });
    $('.navbar .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(150).slideDown();
    }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
    });
    $(window).scroll(function() {
        if ($(window).scrollTop() > 56) {
            $("header").addClass("headershawdow");
        } else {
            $("header").removeClass("headershawdow");
        }
    });
    $('header a[href^=#]').bind("click", jump);

    if (location.hash) {
        setTimeout(function() {
            $('html, body').scrollTop(0).show()
            jump()
        }, 0);
    } else {
        $('html, body').show()
    }
    // $('header a[href^=#]').click(function(){
    //    setTimeout(function(){
    //         $('html, body').scrollTop(0).show()
    //         jump()
    //     }, 0);
    //  });      
    // var pathName = window.location.pathname;
    // var url = "http://redtest.in/cluster/career.html#section4";
    // var hash = url.substring(url.indexOf('#')); 
    // $("#navbarDropdown").click(function(){
    // $('html, body').animate({
    //   scrollTop: $(url).offset().top
    // }, 2000);
    // }); 


});

var jump = function(e) {
    console.log('in');
    if (e) {
        var target = $("header").attr("href");
        // alert(target);
    } else {

        var target = location.hash;
        // alert(target);
    }

    $('html,body').animate({
        scrollTop: $(target).offset().top
    }, 1000, function() {
        location.hash = target;
    });
}

function initMap() {
    // The location of Uluru
    var currentlat = { lat: 12.8989455, lng: 77.6303789 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: currentlat
    });

    var marker = new google.maps.Marker({
        position: currentlat,
        map: map,
        title: 'CLUSTR(Tally Analytics Pvt Ltd.)'
    });
}

// swiper-container
    var swiper = new Swiper('#tab-1 .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 2,
        slidesPerColumn: 1,
        pagination: {
            el: '#tab-1 .swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            // when window width is <= 640px
            640: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 5,
                spaceBetween: 0
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 0
            }
        }
    });
    var swiper = new Swiper('#tab-2 .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 2,
        slidesPerColumn: 2,
        pagination: {
            el: '#tab-2 .swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            640: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 5,
                spaceBetween: 0
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 0
            }
        }
    });
    var swiper = new Swiper('#tab-4 .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 2,
        slidesPerColumn: 1,
        pagination: {
            el: '#tab-4 .swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            640: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 5,
                spaceBetween: 0
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 0
            }
        }
    });
    var swiper = new Swiper('#tab-3 .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 2,
        slidesPerColumn: 2,
        pagination: {
            el: '#tab-3 .swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            640: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 5,
                spaceBetween: 0
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 0
            }
        }
    });
    var swiper = new Swiper('#tab-5 .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 2,
        slidesPerColumn: 2,
        pagination: {
            el: '#tab-5 .swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            640: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerColumn: 1
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 5,
                spaceBetween: 0
            },
            1024: {
                slidesPerView: 5,
                spaceBetween: 0
            }
        }
    });


// active menu 

// $( '#navbar-template .navbar-nav a' ).on( 'click', function () {
//   $( '#navbar-template .navbar-nav' ).find( 'a.active' ).removeClass( 'active' );
//   $( this ).parent( 'a' ).addClass( 'active' );
// });